package it.softwave.examples.greetingservice.controller;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import it.softwave.examples.greetingservice.model.Greeting;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class GreetingController extends BaseController{
    private static final String template = "Hello %s!";
    private static final String goodbyeTemplate = "Good Bye %s!";



    @GetMapping(path ="/greeting/name/{name}")
    @HystrixCommand(commandKey = "greeting",
            fallbackMethod = "fallbackGreeting",
            ignoreExceptions = { RuntimeException.class })
    public Greeting greeting(@PathVariable("name") String name) {

        return new Greeting(counter.incrementAndGet(), String.format(template, name));

    }

}
