package it.softwave.examples.greetingservice.controller;

import it.softwave.examples.greetingservice.model.Greeting;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.atomic.AtomicLong;

@Slf4j
public class BaseController {

    protected static final AtomicLong counter = new AtomicLong();

    public Greeting fallbackGreeting(String name){
        log.info("PING FALLBACK!");
        return new Greeting(counter.incrementAndGet(), "Fallback for ".concat(name));
    }
}
