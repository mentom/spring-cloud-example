package it.softwave.examples.greetingclient;

import it.softwave.examples.greetingclient.client.IGreetingFeignClient;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableDiscoveryClient
@EnableCircuitBreaker
@EnableFeignClients(basePackageClasses = IGreetingFeignClient.class)
//@EnableHystrix
@EnableHystrixDashboard
public class GreetingClientApplication {


    public static void main(String[] args) {
        SpringApplication.run(GreetingClientApplication.class, args);
    }
}
