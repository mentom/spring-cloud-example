package it.softwave.examples.greetingclient.client;

import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(value="greeting-service", fallback = GreetingFallback.class)
@RibbonClient(name = "greeting-service")
public interface IGreetingFeignClient extends IGreeting{

}
