package it.softwave.examples.greetingclient.controller;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import it.softwave.examples.greetingclient.client.GreetingFallback;
import it.softwave.examples.greetingclient.client.IGreetingFeignClient;
import it.softwave.examples.greetingclient.model.Greeting;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@Slf4j
public class FeignClientController {

    private final IGreetingFeignClient greetingClient;

    @LoadBalanced
    @Bean
    RestTemplate getRestTemplate() {
        return new RestTemplate();
    }

    @Autowired
    RestTemplate restTemplate;

    public FeignClientController(IGreetingFeignClient greetingClient) {
        this.greetingClient = greetingClient;
    }

//    @Override
    @GetMapping(path ="/hello/name/{name}")
    @HystrixCommand(commandKey = "getGreeting",
            fallbackMethod = "fallbackGreeting",
            ignoreExceptions = { RuntimeException.class })
    public Greeting getGreeting(@PathVariable("name") String name) {
        log.info("Asking for greeting");
        try {
            return greetingClient.getGreeting(name);
        } catch (Exception e) {
            throw new RuntimeException();
        }
    }

//    @Override
    @GetMapping(path ="/goodbye/name/{name}")
    @HystrixCommand(commandKey = "getGoodbye",
            fallbackMethod = "fallbackGreeting",
            ignoreExceptions = { RuntimeException.class })
    public Greeting getGoodbye(@PathVariable("name") String name) {
        log.info("Asking for greeting");
        try {
            return greetingClient.getGoodbye(name );
        } catch (Exception e) {
            throw new RuntimeException();
        }
    }
    public Greeting fallbackGreeting(String name){
        GreetingFallback greetingFallback = new GreetingFallback();
        return greetingFallback.getGreeting(name);

    }

}
