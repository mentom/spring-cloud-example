package it.softwave.examples.greetingclient.client;

import it.softwave.examples.greetingclient.model.Greeting;
import org.springframework.stereotype.Component;

@Component
public class GreetingFallback implements IGreetingFeignClient {

    @Override
    public Greeting getGreeting(String name) {
        Greeting greeting = new Greeting(1, "No greeting for ".concat(name).concat("... I'm sorry"));
        return greeting;
    }

    @Override
    public Greeting getGoodbye(String name) {
        Greeting greeting = new Greeting(1, "No goodbye for ".concat(name).concat("... I'm sorry"));
        return greeting;
    }
}
