package it.softwave.examples.greetingclient.client;

import it.softwave.examples.greetingclient.model.Greeting;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

public interface IGreeting {
    @GetMapping(value = "/greeting/name/{name}")
    Greeting getGreeting(@PathVariable("name") String name);

    @GetMapping(value = "/goodbye/name/{name}")
    Greeting getGoodbye(@PathVariable("name") String name);

}
