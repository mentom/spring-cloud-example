<h1>Spring Cloud Test Project</h1>

<h2>Finalità.</h2>
<p>Il fine è quello di testare le configurazioni dei vari componenti architetturali messi a disposizoine da Spring Boot e Spring Cloud</p>

<h2>Lista dei moduli.</h2>
<ul>
<li>config-server</li>
<li>greeting-client</li>
<li>greeting-service</li>
<li>histrix-dashboard</li>
<li>spring-boot-admin</li>
<li>zuul-implementation</li>
</ul>

<h2>Descrizione dei moduli.</h2>

<h3>Config/Discovery-Server (Eureka)</h3>
<p><b>Componente architetturale Spring-Cloud</b> che consente la registrazione dei microservizi ed il discovery.</p>

<h3>Greeting-Client</h3>
<p>Client di backend attraverso cui invocare i servizi del <b>greeting-service</b> in base all'endpoint invocato</p>

<h3>Greeting-Service</h3>
<p>Microservizio che genera i saluti: hello o goodbye in base all'endpoint invocato.</p>

<h3>Histrix Dashboard</h3>
<p>Componente <b>Spring Cloud</b> per monitorare il carico dei servizi.</p>
<h4>Utilizzo</h4>
<p>
Da browser accedere all'endpoint:
</p>
<code>http://localhost:7979/hystrix</code>
<p>
Inserire l'URL:
</p>
<code>localhost:8094/actuator/hystrix.stream</code>
<p><b>Nota bene:</b> lo stream histrix sarà attivo solo dopo aver avviato il microservice <b>greeting-service</b></p>

<h3>Spring-Boot-Admin-Test</h3>
<p>Componente architetturale che consente di monitorare i microservizi connessi al Discovery Service accedendo all'url:</p>
<code>http://localhost:8080/</code>

<h3>Zuul-Implementation<h3>
<p>Componente architetturale <b>Spring Cloud</b> che implementa le funzionalità di API Gateway e Load Balancer</p>

<h2>Funzionamento<h2>
<ul>
<li>Avviare tutti i microservizi</li>
<li>Invocare gli endpoint su ZUUL e controllarne le risposte:</li>
    <ul>
        <li><code>http://localhost:8081/greetings/hello/name/foo</code></li>
        <li><code>http://localhost:8081/greetings/goodbye/name/bar</code></li>
    </ul>
<ul>
<li>Accedere alla Histrix Dashboard per monitorare il carico</li>
<li>Accedere a Spring Boot Admin per monitorare il traffico e lo status dei microservizi</li>